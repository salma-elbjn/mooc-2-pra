# Exercice de recherche dans l'historique GitLab

Ce fichier est utile à l'exercice sur l'utilisation de l'historique GitLab.
Il a été modifié à plusieurs reprises pour apporter des corrections ou du contenu supplémentaire.

**Liste ordonnée** :
1. Item
2. Item
3. Item

**Liste non-ordonnée** :
* Item
* Item
* Item

## Welcome Python

```python
#!/usr/bin/python3
import sys

if len(sys.argv) > 1:
    print("Hello " + sys.argv[1])
else:
    print("Hello")
```

