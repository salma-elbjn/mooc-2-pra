# À faire vous-même

Avant d'aborder réellement ce que vous devez faire dans ce Module 1 et les suivants, vous devez vous assurez :

1. d'avoir créé un espace gitlab personnel sur lequel vous pourrez mettre vos ressources 
2. de pouvoir accéder au [Forum support de cette formation](https://mooc-forums.inria.fr/moocnsi/c/mooc-2/191)  

Les objectifs de ce Module 1 sont :

1. Créer ou _emprunter_ une ressource élève, une activité
2. Réaliser pour cette activité une fiche professeur au format markdown comme celles présentées dans les exemples
3. Déposer ce fichier markdown sur votre espace gitlab
4. Déposer aussi, si cela est pertinent dans votre cas, la ressource élève (pour certain.es la ressource _empruntée_ est disponible sur le web et donc ne nécessite pas de réaliser cette étape) 
5. Se rendre sur le Forum pour y mettre un _post_ pour annoncer le travail fait. D'autres pourront alors accéder à votre travail, le commenter, vous faire des retours
6. De votre côté, regardez ce que d'autres ont proposé, commentez, échangez avec les auteur-es.

Vous avez pris connaissance de toute cela ? Alors à vous ! Le travail sollicité est le suivant :

1. Créer ou _emprunter_ une ressource élève, une activité
2. Réaliser pour cette activité une fiche professeur au format markdown comme celles présentées dans les exemples
3. Déposer ce fichier markdown sur votre espace gitlab
4. Déposer aussi, si cela est pertinent dans votre cas, la ressource élève (pour certain.es la ressource _empruntée_ est disponible sur le web et donc ne nécessite pas de réaliser cette étape) 
5. Se rendre sur le Forum pour y mettre un _post_ pour annoncer le travail fait. D'autres pourront alors accéder à votre travail, le commenter, vous faire des retours
6. De votre côté, regardez ce que d'autres ont proposé, commentez, échangez avec les auteur-es.
